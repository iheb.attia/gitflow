# Gitflow


## Why we need a gitflow?
Working with teams can be quite challenging if there is no process in place to review and check code before it is merged.
The Gitflow system is based on Git's ability to manage branches.
The general idea behind this system is to have several separate branches that always exist, each for a different purpose:

| Master                | Develop                 | Feature            | Release              | Hotfix |
|-----------------------|-------------------------|--------------------|----------------------|--------|
| The production branch | Development base branch |Current developments| Complete feature set |Urgent bug fixes |


## Init Develop
So the idea is to have two major branches named **Master** & **Develop**

![Semantic description of image](./images/master-develop.png)
```
git checkout master
git checkout -b develop
```
## Developing a feature with Gitflow
The development of a feature is done on the branch dedicated to it.
You can also develop several functionalities in parallel, and this is often the case when working in a team.
![Semantic description of image](./images/master-develop-feature.png)

```
git checkout develop
git checkout -b feature/25
```
## Create a new release
Once develop has acquired enough functionality for release (or a predefined release date is approaching),
create a release branch from develop.

![Semantic description of image](./images/release.png)

```
git checkout develop
git checkout -b release/1.0
git push --set-upstream release/1.0
git checkout master
git merge release/1.0
```
## Hotfix
![Semantic description of image](./images/hotfix.png)

```
git checkout master
git checkout -b hotfix/1.0
```
After Fixing the bug and comminting the changes, you merge the hotfix into master and develop.
```
git checkout master
git merge hotfix/1.0
git checkout develop
git merge hotfix/1.0
```


## Final gitflow
In order to visualize the interaction of all these branches, you can find the diagram below:
![Semantic description of image](./images/gitflow.png)
